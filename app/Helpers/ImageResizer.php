<?php

namespace App\Helpers;

use Image;

class ImageResizer
{
    public static function fit($path, $width, $height) {
        return self::fill($path, $width, $height);
    }

    public static function scaleWidth($path, $width, $height) {
        return Image::cache(function($image) use ($path, $width) {
            $image->make($path)->widen($width, function ($constraint) {
                $constraint->upsize();
            });
        }, 10, true);
    }
   
    public static function scaleHeight($path, $width, $height) {
        return Image::cache(function($image) use ($path, $height) {
            $image->make($path)->heighten($height, function ($constraint) {
                $constraint->upsize();
            });
        }, 10, true);
    }
   
    public static function fill($path, $width, $height) {
        $image = Image::make($path);
        $cropRatio = $width / $height;
        $imageRatio = $image->width() / $image->height();

        $cropRatio > $imageRatio ? $width = null : $height = null;

        return Image::cache(function($image) use ($path, $width, $height) {
            $image->make($path)->resize($width, $height, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }, 10, true);
    }

    public static function crop($path, $width, $height) {
        return Image::cache(function($image) use ($path, $width, $height) {
            $image->make($path)->fit($width, $height, function ($constraint) {
                $constraint->upsize();
            });
        }, 10, true);
    }
}
