<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use App\Helpers\ImageResizer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Response;

class ResizeController extends BaseController
{
    public function __invoke(Request $request, $image)
    {
        $validator = Validator::make($request->all(), [
            'width' => 'required|integer|min:1',
            'height' => 'required|integer|min:1',
            'method' => ['required', Rule::in(['fit', 'scaleWidth', 'scaleHeight', 'fill', 'crop'])],
            'format' => ['nullable', Rule::in(['jpg', 'png', 'webp'])]
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            if ($exists = Storage::disk()->exists($image)) {
                extract($request->all());

                $result = ImageResizer::$method(Storage::disk()->path($image), $width, $height);

                switch ($format) {
                    case 'jpg':
                        $mimeType = 'image/jpg';
                        break;
                    case 'png':
                        $mimeType = 'image/png';
                        break;
                    case 'webp':
                        $mimeType = 'image/webp';
                        break;
                    default:
                        // format and mime type is determined from image
                        $mimeType = $result->mime();
                        break;
                }

                return Response::make($result->encode($format), 200, ['Content-Type' => $mimeType]);
            } else {
                return abort(404);
            }
        }
    }
}
